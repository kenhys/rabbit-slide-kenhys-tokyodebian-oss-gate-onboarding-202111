# OSS Gate\\nオンボーディング\\n実施報告

subtitle
:  Debianプロジェクトを題材にした取り組みの紹介

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  2021年11月 東京エリア・関西合同Debian勉強会

allotted-time
:  15m

theme
:  .

# スライドは公開済みです

* OSS Gateオンボーディング実施報告 - Debianプロジェクトを題材にした取り組みの紹介
  * <https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-oss-gate-onboarding-202111/>

# 本日の内容

* OSS Gateオンボーディング実施報告
  * そもそもOSS Gateとは何?
  * OSS Gateオンボーディングとは何?
  * Debianプロジェクト向けに実施したこと

# OSS Gateとは何?

![https://oss-gate.github.io/](images/oss-gate.png)

* OSS開発に参加する人を増やす取り組み
  
# OSS Gateオンボーディング?

![https://oss-gate.github.io/on-boarding/](images/onboarding.png)

* 「**継続的に**OSSを開発する人」を増やすことを目指す取り組み

# オンボーディング?

* 一般的に企業で入社後の研修などを指す
  * 新しくはいってきた人が活躍できるようにもろもろ支援する
    * 例: 組織の考えを理解するのが大事なら、その機会を設ける
    * 例: 前提となる必要な知識・スキルがあれば、その習得を支援する

# OSS Gateオンボーディング?

* 新しくはいってきた人が対象OSSで力を発揮できるようにもろもろ支援する
  * 例: 対象OSSの開発体制やポリシーの理解が必要ならその機会を設ける
  * 例: 対象OSSで特定のツールを採用しているならその使い方を伝授する

# OSS Gateオンボーディングのしくみ

* OSS開発参加者が増えて欲しい人(先輩)とOSS開発に参加したい人(新人)をマッチング
* 先輩が**業務時間を使って**新人と一緒に開発する
* 企業はオンボーディングを先輩の業務とすることでスポンサーする

# OSS Gateオンボーディング

* それDebianプロジェクト向けにやってみよう!

# Debianプロジェクトの課題

**(注:個人の主観です)**

* 人的リソースが足りていない
  * <https://salsa.debian.org/freexian-team/project-funding> のようにDebianにとって価値あるプロジェクトに資金援助する仕組みはある(実施済1件、採択1件、提案中0件)
  * <https://salsa.debian.org/debian/grow-your-ideas> Grow your ideas for Debian Project
* 野生のDebian開発者は(なかなか)生えてこない
* だんだん参加者が高齢化していく 👴👵

# 募集内容を考えてみた

<https://oss-gate.github.io/on-boarding/proposals/2021-08/kenhys-maintain-debian-packages/>

* 新規パッケージングコース
  * ITPからRFS、スポンサーアップロードに取り組む
* 既存の不具合修正コース
  * bugs.d.oを活用して既知の問題に取り組む
* mentors.d.n改善コース
  * 登録されているissueの問題に取り組む

# 支援期間後の新人に期待すること

* 新規パッケージングコース
  * 支援期間終了後も新規パッケージングを相談しながら進められる
* 既存の不具合修正コース
  * 支援期間終了後もbugs.d.oを活用して既存の不具合にフィードバックしたり修正したりできる
* mentors.d.n改善コース
  * 支援期間終了後も未着手のissueを相談しながら継続的に修正できる

# 対象を公募(7月)

* debian-usersとubuntu-jpで募集
  * <https://lists.debian.or.jp/pipermail/debian-users/2021-July/000738.html>
  * <https://lists.ubuntu.com/archives/ubuntu-jp/2021-July/006451.html>
* 募集結果
  * 応募5件
  * 問い合わせ1件(どこからはじめたらいいかアドバイスを求める内容)

# 新規パッケージングコース採択

* 支援対象: @sivchariさん
* 支援期間: 8月〜10月上旬
  * 全9回(週1回ペース)
* 支援内容: slack-termのパッケージングをすすめた

# 進め方

* 毎週1回2h一緒に作業する
* 作業するときは日報をつける
  * その記録もOSS Gateオンボーディングのサイトで公開
* 途中でふりかえりの機会を設ける
  * 継続的にできるようにという目的にあっていそうか確認
* 最後に支援期間を通じたふりかえり実施

# 支援期間内の成果

* slack-term (ITP: #994283 作業中。依存パッケージがまだ)
  * golang-github-lithammer-fuzzysearch (Debian unstable入り)
  * golang-github-0xax-notificator (new queue審査待ち)
  * golang-github-erroneousboat-termui (ITP: #994282 作業中)
  * golang-github-slack-go-slack (ITP: #993615 作業中)
  
# 現在の状態(継続中!!)

![](images/ddpo.png){:relative-height="100"}

# 支援期間中に意識したこと(1)

* 今後も継続できるように知見伝えること
  * 支援期間内にパッケージング完了させることが大事ではない
  * 依存パッケージを1つ1つパッケージングして経験値を積んでもらった(今後も同様にできそうと自信をもてる)
  * 新規リリースに追従する作業も体験(golang-github-lithammer-fuzzysearch)してもらった

# 支援期間中に意識したこと(2)

* パッケージングを通じて見つけた問題をフィードバックすること
  * アップストリームへのフィードバック 3件 (うちPR 1件)
  * dh-make-golangのバグ報告1件 (#992610) 0.5.0-1で修正された

# 工夫したこと(1)

* 毎週のミーティングは <https://meet.jit.si/>
  * Jitsiのチャットで参考URL等を都度共有
  * 後から参照できるように、共有した内容は別途テキストにまとめた
    * <https://github.com/oss-gate/on-boarding/blob/main/proposals/2021-08/kenhys-maintain-debian-packages/references-2021-08.md>

# 工夫したこと(2)

* VPS(ConoHa)を用意してもらって一緒に作業する環境を整えた
  * @sivchariさんが使い慣れているため
  * 事前のセットアップ方法は共有しておいた
  * screenでセッションを共有するようにした
    * ブラウザでの画面共有だと文字小さすぎて見えずにつらい問題も解決

# メンターとしてどう感じたのか?

* 一定期間一緒に作業することで、暗黙知を共有・解説で知見を伝えられた
* 調べ直す機会が頻繁にあり、古くなっていた知識を更新できた
* 新コントリビューター向けのドキュメントが必要なのでは?

# 最終レポート

* OSS Gateオンボーディング第一回を実施 - Debianコントリビューターを1人増やせた！
  * <https://www.clear-code.com/blog/2021/10/8/on-boarding-2021-08.html>
* debianのパッケージングができるようになった夏
  * <https://note.com/sivchari/n/ndd8d02477759>

# さいごに

* <https://oss-gate.github.io/on-boarding/proposals/2021-08/kenhys-maintain-debian-packages/>
  * 日報、ふりかえり、最終レポートの記録も↑からたどれるようになっています
* 新コントリビューターガイドを書き始めました
  * 技術書典12 <https://techbookfest.org/event/tbf12> にだします(申込み審査待ち)
